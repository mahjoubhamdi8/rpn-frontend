import './App.css';
import {BrowserRouter,Route, Routes } from 'react-router-dom';
import StackList from './components/StackList';
import EditStack from './components/EditStack';
import 'bootstrap/dist/css/bootstrap.min.css';
import FooterComponent from './components/FooterComponent';
import HeaderComponent from './components/HeaderComponent';

function App() {
  return (
    <BrowserRouter>
      <HeaderComponent/>
      <div className="container">
      <Routes>
        {/* Define your routes here */}
        <Route path="/" exact element={<StackList/>} />
        <Route path="/edit-stack/:id" exact element={<EditStack />} />
        {/* Add more routes as needed */}
      </Routes>
      </div>
      <FooterComponent/>
    </BrowserRouter>
  );
}

export default App;
