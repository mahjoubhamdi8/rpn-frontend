import React from 'react';

const OperationButtons = ({ handleOperation }) => {
  return (
    <div>
      <button
        className="btn btn-primary mx-2"
        onClick={() => handleOperation('add')}
      >
        +
      </button>
      <button
        className="btn btn-primary mx-2"
        onClick={() => handleOperation('sous')}
      >
        -
      </button>
      <button
        className="btn btn-primary mx-2"
        onClick={() => handleOperation('mul')}
      >
        X
      </button>
      <button
        className="btn btn-primary mx-2"
        onClick={() => handleOperation('div')}
      >
        /
      </button>
    </div>
  );
};

export default OperationButtons;