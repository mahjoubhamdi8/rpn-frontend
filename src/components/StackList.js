import React, { useState, useEffect } from 'react';
import StackService from '../services/StackService';
import { useNavigate } from 'react-router-dom';

const StackList = () => {
  const [stacks, setStacks] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    fetchStacks();
  }, []);

  const fetchStacks = () => {
    StackService.getStacks().then((res) => {
      setStacks(res.data);
    });
  };

  const handleDeleteStack = (id) => {
    StackService.deleteStack(id).then(() => {
      fetchStacks(); 
    }).catch(error => {
      console.error('Error deleting stack:', error);
    });
    };

    const handleClearStack = (id) => {
      StackService.clearStack(id).then(() => {
        fetchStacks(); 
        navigate(`/edit-stack/${id}`);
      }).catch(error => {
        console.error('Error deleting stack:', error);
      });
      };

    const handleAddStack = () => {
        StackService.createStack()
          .then(() => {
            fetchStacks(); 
            
          })
          .catch(error => {
            console.error('Error adding stack:', error);
          });
      };

    const viewStack = (id) => {
        navigate(`/edit-stack/${id}`);
      };
  

    return (
        <div className="container">
          <h2>List of Stacks</h2>
          <ul className="list-group">
            {stacks.map((stack, index) => (
              <li key={index} className="list-group-item d-flex justify-content-between align-items-center">
                Stack {index + 1}
                <div>
                  <button
                    className="btn btn-info mr-2"
                    onClick={() => viewStack(stack.id)}
                  >
                    View
                  </button>
                  <button
                    className="btn btn-warning mx-2"
                    onClick={() => handleClearStack(stack.id)}
                  >
                    Reset
                  </button>
                  <button
                    className="btn btn-danger"
                    onClick={() => handleDeleteStack(stack.id)}
                  >
                    Delete
                  </button>
                </div>
              </li>
            ))}
          </ul>
          <div className="mt-4">
            <h2>Add New Stack</h2>
            <button
              className="btn btn-primary"
              onClick={() => handleAddStack()}
            >
              Add Stack
            </button>
          </div>
        </div>
      );
    };
    
    export default StackList;