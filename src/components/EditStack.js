import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import StackService from '../services/StackService';
import OperationButtons from './OperationButtons';
import { useNavigate } from 'react-router-dom';

const EditStack = () => {
  const navigate = useNavigate();

  const { id } = useParams();
  const [stack, setStack] = useState(null); 
  const [inputValue, setInputValue] = useState('');

  useEffect(() => {
    fetchStack(id)
  }, [id]);

  const fetchStack = (id) => {
    StackService.getStackById(id)
      .then((res) => {
        setStack(res.data);
      })
      .catch((error) => {
        console.error('Error fetching stack:', error);
        setStack(null); 
      });
  };

  if (!stack) {
    return <div>Loading...</div>;
  }

    const formattedStackValues = stack.stack_values.join(', ');

    const handleAddValue = () => {
    const newValue = parseFloat(inputValue);

    if (!isNaN(newValue)) {
      StackService.pushValuetoStack(newValue , stack.id).then((res) => {
      })
      .catch((error) => {
        console.error('Error adding value:', error);
      }); 
      const updatedStack = { ...stack, stack_values: [...stack.stack_values, newValue] };
      setStack(updatedStack);
      setInputValue('');

    }
  };

  const handleOperation = (op) => {
    StackService.applyOperation(op ,id ).then(() => {
        fetchStack(id)
    }).catch(error => {
        console.error('Error applying operation:', error);
      });
  };
  const cancel = () => {
    navigate('/');
  };
  return (
    <div className="container mt-4">
      <div className="row align-items-center">
        <div className="col">
          <input
            type="text"
            value={inputValue}
            onChange={(e) => setInputValue(e.target.value)}
            className="form-control"
            placeholder="Enter a value"
          />
        </div>
        <div className="col-auto">
          <button onClick={handleAddValue} className="btn btn-secondary">
            Add Value
          </button>
        </div>
        <div className="col">
          <OperationButtons handleOperation={handleOperation} />
        </div>
      </div>
      <div  className="mt-4">
        <h3>Stack Values: [ {formattedStackValues} ]</h3>
        <div className="col-auto mt-4 ">
          <button className="btn btn-danger" onClick={cancel}> Back</button>
        </div> 
        </div>
    </div>
  );
};

export default EditStack;