import axios from 'axios';

const STACK_API_BASE_URL = "http://localhost:8080/rpn";

class StackService {

    getStacks(){
        return axios.get(STACK_API_BASE_URL + '/stack');
    }

    createStack(stack){
        return axios.post(STACK_API_BASE_URL +'/stack');
    }

    getStackById(stackId){
        return axios.get(STACK_API_BASE_URL + '/stack/'+ stackId);
    }

    pushValuetoStack(value, stackId){
        return axios.post(STACK_API_BASE_URL + '/stack/' + stackId, null , {
            headers: {
              'Content-Type': 'application/json', 
            },
            params: {
              value: value, 
            },
          });
    }


    deleteStack(stackId){
        return axios.delete(STACK_API_BASE_URL + '/stack/' + stackId);
    }

    clearStack(stackId){
        return axios.put(STACK_API_BASE_URL + '/stack/' + stackId);
    }
    
    applyOperation(op , stackId){
        return axios.post(STACK_API_BASE_URL + '/op/'+ op + '/stack/' + stackId);
    }
}
StackService = new StackService();


export default StackService